open Ca

module M = Make (NaiveCA)

let color_of_string s =
  match s with
  | "white" -> (255,255,255)
  | "black" -> (0,0,0)
  | "yellow" -> (0,0,0)
  | "red" -> (255,0,0)
  | "magenta" -> (255,0,255)
  | "green" -> (0,255,0)
  | "blue" -> (0,0,255)
  | "cyan" -> (0,0,255)
  | _ ->
     let x = int_of_string s in
     (x lsr 16,x lsr 8 mod 255,x mod 255)

let speclist =
  let open Arg in
  [
    ("--sleep",
     (Float (fun x -> sleep_time := x)),
     "sleeping time between two refresh");
    ("--file",
     (String (fun s -> init_file := Some s)),
     "initiation file");

    ("--alive",
     (String (fun s -> alive_color := color_of_string s)),
     "alive cells color");

    ("--dead",
     (String (fun s -> dead_color := color_of_string s)),
     "dead celles color");

    ("--random_color",
     Set random_color,
     "set random color")
  ]

let anon_fun _ = ()
let usage_message = "usage : ./gol.exe [--sleep=FLOAT] [--file=FILE] \
                     [--alive=COLOR] [--dead=COLOR] [--random_color]"

let window_w = 600
let window_h = 600

(* Count the numbers of alive cells *)
let count_alive_cells xs =
  List.fold_left
    (fun acc status -> match status with Alive -> acc+1 | Dead -> acc) 0 xs

(* Rules for the famous game of life *)
let gol_rules status neighbours =
  let r = count_alive_cells neighbours in
  match status with
  | Dead ->
     if r = 3 then Alive else Dead
  | Alive ->
     if r = 2 || r = 3 then Alive else Dead

let init_config () = {
    sleep_time = !sleep_time;
    init_file = !init_file;
    width = 25;
    height = 25;
  }

let main =
  Arg.parse speclist anon_fun usage_message;
  M.init window_w window_h;
  let config = init_config () in
  (try M.play config gol_rules
   with Graphics.Graphic_failure _ -> ());
  exit 0
