(*-------------------------- Rules -------------------------------*)

(*
 *  a | b | c
 * ---+---+---
 *  d | e | f
 * ---+---+---
 *  g | h | i
 *
 * The input list for a rule is [ a; b; c; d; f; g; h ; i ]
 * Note that we don't include element e
 *)

(*----------------------------------------------------------------*)


type cell = Alive | Dead

type rules = cell -> cell list -> cell

(* Signature module for a grid along with a `next` functions *)
module type S = sig
  type grid

  type t = {
      grid: grid;
      width: int;
      height: int;
    }

  (* Create an empty grid *)
  val empty : width:int -> height:int -> t
  (* Getter *)
  val (-?) : t -> int * int -> cell
  (* Setter *)
  val (<--) : t -> int * int * cell -> t
  (* Returns the next configuration of the grid *)
  val next_state : t -> rules -> t
end

module NaiveCA = struct
  type t = {
      grid: grid;
      width: int;
      height: int
    }
  and grid  = cell array array

  let empty ~width ~height =
    {
      grid   = Array.make_matrix width height Dead;
      width  = width;
      height = height
    }

  (*----------------------------------------------------------*)

  (* Getter *)
  let get_in_bound i high : int =
    if i < 0 then high-1
    else if i >= high then 0
    else i

  let get t ~x ~y =
    let x = get_in_bound x t.width in
    let y = get_in_bound y t.height in
    t.grid.(y).(x)

  let (-?) t (x,y) = get t ~x ~y

  (*----------------------------------------------------------*)

  (* Setter *)
  let set t ~x ~y s =
    let x = get_in_bound x t.width in
    let y = get_in_bound y t.height in
    t.grid.(y).(x) <- s;
    t

  let (<--) t (x,y,s) = set t ~x ~y s

  (*----------------------------------------------------------*)

  (* Get a list of the 8 neighbours + the cell at position (x,y) *)
  let get_neighbours t ~x ~y : cell list =
    let relative_poss = [
        (-1,1);(0,1);(1,1);(-1,0);(1,0);(-1,-1);(0,-1);(1,-1)
      ] in
    let poss = List.map (fun (a,b) -> (x+a,y+b)) relative_poss in
    List.map ((-?) t) poss

  (* Compute the next state of the cell *)
  let next_cell t ~x ~y rules =
    let neighbours = get_neighbours t ~x ~y in
    rules (t -? (x,y)) neighbours

  (* Return the next configuration of the grid *)
  let next_state t rules =
    (* First we need to create a new grid as we cannot do
     * in-place modification *)
    let new_t = empty ~width:t.width ~height:t.height in

    (* For each cell in the old grid, fill the new grid with the next
     * cell according to the rules. *)
    for x=0 to t.width do
      for y=0 to t.width do
        new_t <-- (x, y, next_cell t ~x ~y rules) |> ignore
      done
    done;

    (* Returns the new grid *)
    new_t

end

(*----------------------------------------------------------*)

(* Configuration with default values *)
let alive_color = ref (0,0,0)
let dead_color  = ref (255,255,255)
let random_color = ref false
let random_grid = ref true
let sleep_time : float ref = ref 0.1
let init_file : string option ref = ref None

(*----------------------------------------------------------*)

type config = {
    sleep_time: float;
    init_file: string option;
    width: int;
    height: int
  }

module Make (CA : S) : sig
  val init: int -> int -> unit
  val play: config -> rules -> unit
end = struct
  open Graphics
  open CA

  let _ =  Random.self_init ()

  let init_graph w h =
    open_graph (Printf.sprintf " %dx%d" w h)

  let init window_w window_h =
    init_graph window_w window_h

  let randomize x =
    let y = x + (Random.int 100) - 50 in
    if y < 0 || y > 255 then x else y

  let draw t =
    let wx = size_x () / t.width in
    let wy = size_y () / t.height in
    for x=0 to t.width do
      for y=0 to t.height do
        let (r,g,b) =
          match t -? (x,y) with
          | Dead ->
             !dead_color
          | Alive ->
             !alive_color
        in
        let (r,g,b) =
          if !random_color && t -? (x,y) = Alive then
            (randomize r,randomize g,randomize b)
          else
            (r,g,b)
        in
        set_color (rgb r g b);
        fill_rect (x * wx) (y * wy) wx wy
      done
    done

  let (<--) t (x,y,v) = t <-- (x,y,v) |> ignore

  let random_cell () =
    match Random.bool () with
    | false -> Dead
    | true -> Alive

  let initial_grid_random ~width ~height =
    let t = empty ~width ~height in
    for x=0 to width do
      for y=0 to height do
        t <-- (x,y,random_cell ())
      done
    done;
    t

  let read_lines file : string list =
    let ic = open_in file in
    let rec all_lines () =
      try
        let s = input_line ic in
        s :: (all_lines ())
      with End_of_file -> []
    in
    let lines = all_lines () in
    close_in ic;
    lines

  let pattern_as_matrix ~width ~height lines : cell array array =
    (* Make a matrix representing the predefined pattern in the initial grid *)
    let ar = Array.make_matrix height width Dead in

    let int_to_cell_status c =
      match int_of_char c - int_of_char '0' with
      | 0 -> Dead
      | _ -> Alive
    in

    (* Fill the matrix *)
    List.iteri
      (fun y s ->
        String.iteri (fun x c -> ar.(y).(x) <- int_to_cell_status c) s)
      lines;

    ar

  let initial_grid_from_file file ~width ~height =
    (* Read the lines of predefined pattern in the initial grid *)
    let lines = read_lines file in

    (* Create an empty grid *)
    let t  = empty ~width ~height in

    (* Define constants useful for the rest of the function's body *)
    let width2  = width / 2 in
    let height2 = height / 2 in
    let length_of_line  = String.length (List.hd lines) in
    let number_of_lines = List.length lines in

    (* Retrieve the initial pattern as a matrix of cell *)
    let pattern : cell array array =
      pattern_as_matrix ~width:length_of_line ~height:number_of_lines lines in

    (* Fill the grid with the pattern at the center *)
    for x = 0 to length_of_line - 1 do
      for y = 0 to number_of_lines - 1 do
        t <-- (x + width2 - length_of_line / 2,
               y + height2 - number_of_lines / 2,
               pattern.(number_of_lines - 1 - y).(x))
      done
    done;
    t

  (* Allow the user to make cells alive by clicking on them *)
  let interact config t new_t =
    let time = Sys.time () in
    let wx = size_x () / t.width in
    let wy = size_y () / t.height in
    if not (button_down ()) then
      false
    else begin
      while button_down () do
        while Sys.time () -. time < config.sleep_time do
          let (x,y) = mouse_pos () in
          if x < size_x () && y < size_y () then
            new_t <-- (x / wx, y / wy, Alive)
        done
      done;
      true
      end

  let play (conf:config) rules =
    let width = conf.width in
    let height = conf.height in

    let initial_grid =
      match conf.init_file with
      | Some file ->
         initial_grid_from_file file ~width ~height
      | None ->
         if !random_grid then
           initial_grid_random ~width ~height
         else
           empty ~width ~height
    in

    let rec loop t =
      draw t;
      let new_t = next_state t rules in
      if not (interact conf t new_t) then
        Unix.sleepf conf.sleep_time
      ;
      loop new_t
    in
    loop initial_grid

end
